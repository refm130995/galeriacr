/** @format */

import _ from "lodash";

const currencies = {
  USD: {
    symbol: "$",
    name: "US Dollar",
    symbol_native: "$",
    decimal_digits: 2,
    rounding: 0,
    code: "USD",
    name_plural: "US dollars",
  }
};

let currenciesArray = [];

_.map(currencies, (curr, idx) => {
  currenciesArray.push({
    value: idx,
    ...curr,
  });
});

export default currenciesArray;
