import React, { PureComponent } from 'react';
import { ViroARSceneNavigator } from 'react-viro';
import ARView from '../ARView';

export default class ARNavigator extends PureComponent {
	render() {
		const { imageUrl } = this.props;
		return (
			<ViroARSceneNavigator
				apiKey="32CD5497-F539-41AB-9575-DBF919B7ED22"
				initialScene={{ scene: ARView, passProps: { imageUrl } }}
			/>
		);
	}
}
