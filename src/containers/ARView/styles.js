import { StyleSheet } from 'react-native';

var styles = StyleSheet.create({
	helloWorldTextStyle: {
		fontFamily: 'Arial',
		fontSize: 72,
		color: '#ffffff',
		textAlignVertical: 'center',
		textAlign: 'center',
	},
});