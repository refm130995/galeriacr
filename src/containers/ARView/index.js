import React, { Component } from 'react';
import { ViroConstants, ViroARScene, ViroText, ViroARPlaneSelector, ViroImage } from 'react-viro';
import styles from './styles';

export default class ARView extends Component {
	constructor(props) {
		super(props);
		this.state = {
			text: 'Initializing AR...',
			viroImageRotation: [-90, -45, 0],
			viroImageWidth: 0.25,
			viroImageHeight: 0.25,
			viroImagePosition: [0, 0, 0],
			isLoadingTextVisible: true,
		};
		this._onInitialized = this._onInitialized.bind(this);
		this._onPlaneSelected = this._onPlaneSelected.bind(this);
		this._onImagePinch = this._onImagePinch.bind(this);
		this._onImageRotate = this._onImageRotate.bind(this);
		this._onImageDrag = this._onImageDrag.bind(this);
	}

	_onImageDrag(dragToPos, source) {
		/*
		this.setState({
		  viroImagePosition: [dragToPos[0], dragToPos[1], 0]
		});
		*/
	}

	_onImagePinch(pinchState, scaleFactor, source) {
		if (pinchState == 3) {
			this.setState({
				viroImageWidth: this.state.viroImageWidth * scaleFactor,
				viroImageHeight: this.state.viroImageHeight * scaleFactor,
			});
			return;
		}
		this._viroImageRef.setNativeProps({
			width: this.state.viroImageWidth * scaleFactor,
			height: this.state.viroImageHeight * scaleFactor,
		});
	}

	_onImageRotate(rotateState, rotationFactor, source) {
		if (rotateState == 3) {
			this.setState({
				viroImageRotation: [
					this.state.viroImageRotation[0],
					this.state.viroImageRotation[1] - rotationFactor,
					this.state.viroImageRotation[2],
				],
			});
			//set to current rotation - rotationFactor.
			return;
		}
		// Use Setnativeprops
		this._viroImageRef.setNativeProps({
			rotation: [
				this.state.viroImageRotation[0],
				this.state.viroImageRotation[1] - rotationFactor,
				this.state.viroImageRotation[2],
			],
		});
	}

	_onPlaneSelected(source) {
		this.setState({
			// text: `H: ${source.height.toFixed(2)} W: ${source.width.toFixed(2)}`,
			isLoadingTextVisible: false,
		});
	}

	_onInitialized(state, reason) {
		if (state == ViroConstants.TRACKING_NORMAL) {
			this.setState({
				text: 'Hello World!',
			});
		} else if (state == ViroConstants.TRACKING_NONE) {
			// TODO: Handle loss of tracking
		}
	}

	render() {
		const { imageUrl } = this.props;
		return (
			<ViroARScene anchorDetectionTypes={['PlanesVertical']} onTrackingUpdated={this._onInitialized}>
				<ViroText
					text={this.state.text}
					scale={[0.5, 0.5, 0.5]}
					position={[0, 0, -1]}
					style={styles.helloWorldTextStyle}
					visible={this.state.isLoadingTextVisible}
				/>
				<ViroARPlaneSelector alignment="Vertical">
					<ViroImage
						ref={component => (this._viroImageRef = component)}
						height={this.state.viroImageHeight}
						width={this.state.viroImageWidth}
						placeholderSource={require('./res/loading.png')}
						source={{ uri: imageUrl }}
						rotation={this.state.viroImageRotation}
						onPinch={this._onImagePinch}
						onRotate={this._onImageRotate}
						onDrag={this._onImageDrag}
						position={this.state.viroImagePosition}
					/>
				</ViroARPlaneSelector>
			</ViroARScene>
		);
	}
}
